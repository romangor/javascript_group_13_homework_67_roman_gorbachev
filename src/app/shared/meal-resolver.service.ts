import { EMPTY, Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { MealService } from './meal.service';
import { mergeMap } from 'rxjs/operators';
import { Meal } from './meal.model';

@Injectable({
  providedIn: 'root'
})
export class MealResolverService implements Resolve<Meal> {

  constructor(private mealService: MealService,
              private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Meal> | Observable<never> {
    const mealId = route.params['id'];
    return this.mealService.fetchMeal(mealId).pipe(mergeMap(meal => {
      if (meal) {
        return of(meal);
      } else {
        void this.router.navigate(['/']);
        return EMPTY;
      }
    }));
  }

}
