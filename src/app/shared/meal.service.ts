import { Meal } from './meal.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Injectable()
export class MealService {
  changeMeals = new Subject<Meal[]>();
  fetchingMeals = new Subject<boolean>();
  mealUpLoading = new Subject<boolean>()
  meals: Meal[] = [];

  constructor(private http: HttpClient) {
  }

  saveMeal(meal: Meal, isEdit: boolean) {
    const body = {
      id: meal.id,
      time: meal.time,
      description: meal.description,
      calories: meal.calories,
      dateMeal: meal.dateMeal,
    }
    this.mealUpLoading.next(true);
    if (isEdit) {
      return this.http.put(`https://projectsattractor-default-rtdb.firebaseio.com/tracker/${meal.id}.json`, body)
        .pipe(tap(() => {
          this.mealUpLoading.next(false);
        }, () => {
          this.mealUpLoading.next(false);
        }));
    } else {
      return this.http.post('https://projectsattractor-default-rtdb.firebaseio.com/tracker.json', body)
        .pipe(tap(() => {
          this.mealUpLoading.next(false);
        }, () => {
          this.mealUpLoading.next(false);
        }));
    }
  }

  getMeals() {
    return this.meals.slice();
  }

  fetchMeals() {
    this.fetchingMeals.next(true);
    this.http.get<{ [id: string]: Meal }>('https://projectsattractor-default-rtdb.firebaseio.com/tracker.json')
      .pipe(map(result => {
        if (result === null) {
          return [];
        }
        return Object.keys(result).map(id => {
          const mealData = result[id];
          return new Meal(id, mealData.time, mealData.description, mealData.calories, mealData.dateMeal);
        });
      }))
      .subscribe(meals => {
        this.meals = meals;
        this.changeMeals.next(this.meals.slice());
        this.fetchingMeals.next(false);
      })
  }

  fetchMeal(id: string) {
    return this.http.get<Meal | null>(`https://projectsattractor-default-rtdb.firebaseio.com/tracker/${id}.json`)
      .pipe(map(result => {
        if (!result) return null;
        return new Meal(id, result.time, result.description, result.calories, result.dateMeal);
      }));
  }

  onRemove(id: string) {
    return this.http.delete(`https://projectsattractor-default-rtdb.firebaseio.com/tracker/${id}.json`);
  }

}
