import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { MealService } from './shared/meal.service';
import { EditMealComponent } from './edit-meal/edit-meal.component';
import { HomeComponent } from './home/home.component';
import { MealsComponent } from './home/meals/meals.component';
import { NotFoundComponent } from './NotFound.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    EditMealComponent,
    HomeComponent,
    MealsComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [MealService],
  bootstrap: [AppComponent]
})
export class AppModule { }
