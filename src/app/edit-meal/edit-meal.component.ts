import { Component, OnInit, ViewChild } from '@angular/core';
import { MealService } from '../shared/meal.service';
import { NgForm } from '@angular/forms';
import { Meal } from '../shared/meal.model';
import { ActivatedRoute, Router } from '@angular/router';


class Data {
}

@Component({
  selector: 'app-edit-calories',
  templateUrl: './edit-meal.component.html',
  styleUrls: ['./edit-meal.component.css']
})
export class EditMealComponent implements OnInit {
  @ViewChild('f') form!: NgForm;

  isEdit = false;
  editId = '';
  loading!: boolean;
  date = new Data();

  constructor(private mealService: MealService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.mealService.mealUpLoading.subscribe((isLoading: boolean) => {
      this.loading = isLoading;
    });
    this.route.data.subscribe(data => {
      const meal = <Meal | null>data.meal;
      if (meal) {
        this.setFormValue({
          mealSelect: meal.time,
          mealDescription: meal.description,
          calories: meal.calories,
          dateMeal: meal.dateMeal,
        });
        this.isEdit = true;
        this.editId = meal.id;
      } else {
        this.setFormValue({
          mealSelect: '',
          mealDescription: '',
          calories: '',
          dateMeal: '',
        });
        this.isEdit = false;
        this.editId = '';
      }
    })
  }

  setFormValue(value: { [key: string]: any }) {
    setTimeout(() => {
      this.form.form.setValue(value);
    });
  }

  addMeal() {
    const id = this.editId || '';
    const meal = new Meal(id, this.form.value.mealSelect, this.form.value.mealDescription, this.form.value.calories, this.form.value.dateMeal);
    const next = () => {
      this.mealService.fetchMeals();
      void this.router.navigate(['']);
    };
    this.mealService.saveMeal(meal, this.isEdit).subscribe(next);
  }
}
