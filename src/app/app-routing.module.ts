import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditMealComponent } from './edit-meal/edit-meal.component';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './NotFound.component';
import { MealResolverService } from './shared/meal-resolver.service';

const routes: Routes = [{path: '', component: HomeComponent},
                        {path: 'home/new', component: EditMealComponent},
                        {path: ':id/edit', component: EditMealComponent,
                          resolve: {meal: MealResolverService}},
                        {path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
