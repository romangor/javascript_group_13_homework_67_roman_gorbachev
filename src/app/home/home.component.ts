import { Component, OnDestroy, OnInit } from '@angular/core';
import { Meal } from '../shared/meal.model';
import { MealService } from '../shared/meal.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {

  meals: Meal[] = [];
  totalCalories!: number;
  mealsChangeSubscription!: Subscription;
  fetchingMealsSubscription!: Subscription;
  loadingMeals: boolean = false;

  constructor(private mealService: MealService) {
  }

  ngOnInit(): void {
    this.meals = this.mealService.getMeals();
    this.mealsChangeSubscription = this.mealService.changeMeals.subscribe(meals => {
      this.meals = meals;
    });
    this.fetchingMealsSubscription = this.mealService.fetchingMeals.subscribe((isLoading: boolean) => {
      this.loadingMeals = isLoading;
    })
    this.mealService.fetchMeals();
  }

  getCalories() {
    this.totalCalories = this.meals.reduce((sum, meal) => {
      return sum + meal.getCalories();
    }, 0);
    return this.totalCalories;
  }

  ngOnDestroy() {
    this.mealsChangeSubscription.unsubscribe();
    this.fetchingMealsSubscription.unsubscribe();
  }
}
