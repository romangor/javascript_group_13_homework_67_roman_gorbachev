import { Component, Input, OnInit } from '@angular/core';
import { Meal } from 'src/app/shared/meal.model';
import { MealService } from '../../shared/meal.service';

@Component({
  selector: 'app-meals',
  templateUrl: './meals.component.html',
  styleUrls: ['./meals.component.css']
})
export class MealsComponent implements OnInit {
  @Input() meal!: Meal;
  loading: boolean = false;

  constructor(private mealService: MealService) {
  }

  ngOnInit(): void {
  }

  onRemove(id: string) {
    this.loading = true;
    this.mealService.onRemove(id).subscribe(() => {
      this.mealService.fetchMeals();
    });
  }
}
